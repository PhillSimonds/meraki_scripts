#!/usr/bin/env Python3

#######################################################################################################################
#
# Overview
# The purpose of this script is to remove static routes from a given Meraki network
#
# Dependencies
# - Python 3.6
# - 'requests' module
# - 'meraki' module
#
#######################################################################################################################

from meraki import meraki
import json
import sys

print("\n This script deletes all static routes on an MX appliance for a given network.\n")

# Define API Key for programmatic access to Meraki Dashboard and HDVO 
# Org ID used to query customer-specific information from Meraki API

apikey = input("What is your Meraki API key: ")

# Get ORG ID

try:
    print("Getting list of organizations from Meraki Cloud")
    orgs = meraki.myorgaccess(apikey)
    print("Available Organizations: ")
    for org in orgs:
        print("\t - " + org["name"])
    while True:
        usr_input = input("For which organization would you like to choose a network to execute the script on?: ")
        if usr_input not in str(orgs):
            print("Invalid Entry. The org name may have been mis-spelled?")
        elif usr_input in str(orgs):
            break
    for org in orgs:
        if org["name"] == usr_input:
            orgid = org["id"]
except TypeError:
    print("Unable to get org data with that API key")
    quit()
            
# Get Network ID

print("Getting list of networks from Meraki Cloud")
networks = meraki.getnetworklist(apikey, orgid)
print(networks)
print("Available Networks:")
for network in networks:
    print("\t - " + network["name"])
while True:
    usr_input = input("On which network would you like to execute the script? ")
    if usr_input not in str(networks):
        print("Invalid Entry. The network name may have been mis-spelled?")
    elif usr_input in str(networks):
        break
for network in networks:
    if network["name"] == usr_input:
        networkid = network["id"]

site_name = usr_input

# Get list of static routes using site ID procured from site name
# Prompt User to verify we want to remove static routes
# Remove Static Routes

print("Getting list of static routes.")
static_routes = meraki.getstaticroutes(apikey, networkid)

if len(static_routes) == 0:
    print("There are no static routes at site " + site_name + ".")
    quit()
else:
    print("The following static routes exist at " + site_name + ": ")
    for route in static_routes:
        print("\t -" + route["subnet"] + " via " + route["gatewayIp"])

usr_input = input("We will now remove the static routes. Is this OK? (type 'yes' or 'no'): ")

while True:
    if usr_input == "yes":
        print("Removing static routes")
        for route in static_routes:
            routeid = route["id"]
            meraki.delstaticroute(apikey, networkid, routeid)
        break
    elif usr_input == "no":
        print("OK. exiting script. Goodbye.")
        quit()
    else:
        usr_input = input("Sorry, that answer was invalid. Please type 'yes' or 'no': ")

if len(static_routes) > 1:
    print("It looks like there are still static routes. after our attempted deletion"
    "Please check the dashboard to make sure everything went according to plan")
else:
    print("Static Routes Successfully Removed")